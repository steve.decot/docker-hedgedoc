# docker-hedgedoc
HedgeDoc (formerly known as CodiMD) is an open-source collaborative markdown editor. With HedgeDoc you can easily collaborate on notes, graphs and even presentations in real-time. All you need to do is to share your note-link to your co-workers, and they’re ready to go.

https://hedgedoc.org/
https://github.com/hedgedoc/hedgedoc
